import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/login',
    method: 'post',
    data
  })
}

export function getInfo(token) {
  return request({
    url: '/vue-admin-template/user/info',
    method: 'get',
    data: { token }
  })
}

export function userRegister(username,password,role) {
  return request({
    url: '/users',
    method: 'post',
    data: { username,password,role }
  })
}
export function userDeleteById(id) {
  return request({
    url: '/user/deleteById'+id,
    method: 'delete',
  })
}


export function userFindAll(currentPage,createBy) {
  return request({
    url: '/user/findAll/'+currentPage+'/10/'+createBy,
    method: 'GET',
  })
}
// user/update


export function userUpdate(ruleForm) {
  return request({
    url: '/user/update',
    method: 'put',
    data:ruleForm
  })
}


export function userFindById(id) {
  return request({
    url: '/user/findById/'+id,
    method: 'GET',
  })
}

export function logout() {
  return request({
    url: '/vue-admin-template/user/logout',
    method: 'post'
  })
}

export function dataManageSave(ruleForm) {
  return request({
    url: '/dataManage/save',
    method: 'post',
    data:ruleForm
  })
}
export function dataManageUpdate(ruleForm) {
  return request({
    url: '/dataManage/update',
    method: 'post',
    data:ruleForm
  })
}

export function dataManageFindById(id) {
  return request({
    url: '/dataManage/findById/'+id,
    method: 'GET',
  })
}
export function dataManageDeleteById(id) {
  return request({
    url: '/dataManage/deleteById/'+id,
    method: 'DELETE',
  })
}

export function dataManageFindAll(currentPage,createBy) {
  return request({
    url: '/dataManage/findAll/'+currentPage+'/10/'+createBy,
    method: 'GET',
  })
}
